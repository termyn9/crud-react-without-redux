import React from "react";
import { Button, ButtonGroup } from 'reactstrap';
import './Table.css';

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false
        };
    }
    edit = () => {
        this.setState({ edit: true });
    };
    remove = () => {
        this.props.deleteBlock(this.props.index)
    };
    save = () => {
        this.props.update(this.refs.newEmail.value, this.refs.newAge.value, this.props.index)
        this.setState({ edit: false });
    };
    outside = () => {
        return (
            <div className="container">
                <div className="row">
                    <input className="col-5 input" defaultValue={this.props.email} disabled='disabled'/>
                    <input className="col-5 input" defaultValue={this.props.age} disabled='disabled'/>
                    <div className="col-2">
                        <ButtonGroup>
                            <Button onClick={this.edit} color="success">
                                Edit
                            </Button>
                            <Button onClick={this.remove} color="danger">
                                Delete
                            </Button>
                        </ButtonGroup>
                    </div>
                </div>
            </div>
        );
    };
    inside = () => {
        return (
            <div className="container">
               <div className="row">
                   <input className="col-5 input" ref="newEmail" defaultValue={this.props.email} />
                   <input className="col-5 input" ref="newAge" defaultValue={this.props.age} />
                   <Button className="col-10 " size="sm" color="primary" onClick={this.save} >
                       Save
                   </Button>
               </div>
            </div>
        );
    };
    render() {
        if (this.state.edit) {
            return this.inside();
        } else {
            return this.outside();
        }
    }
}

export default Table;