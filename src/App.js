import React from "react";
import Table from './components/Table';
import { Button } from 'reactstrap';
import './App.css';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: '',
            fetch: false
        };
  };
  
  add = (text, chislo) => {
    let data = {
      data: {
        email: text,
        age: chislo
      }
    }
        fetch('http://178.128.196.163:3000/api/records', 
          {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          })
    };

    deleteBlock = i => {
      fetch(`http://178.128.196.163:3000/api/records/${i}`,
        {
          method: 'DELETE'
        })
        .then(response => response.json())
    };

    updateText = (email, age, i) => {
        let data = {
          data: {
            email: email,
            age: age
          }
        }
      
        fetch(`http://178.128.196.163:3000/api/records/${i}`, 
                
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          })
    };
  
    getFetch =() =>{
      fetch('http://178.128.196.163:3000/api/records', {
        method:
          'GET'
      })
        .then(response => response.json())
        .then((data) => this.setState({ 
          users: data,
          fetch: !this.state.fetch
        }))
    }
  
    componentDidMount () {
        this.getFetch();
        this.timer = setInterval(() => this.getFetch(), 60000);
    }
  
    componentWillUnmount() {
        clearInterval(this.timer)
    }
  
    eachTask = (item) => {
        return (
            <div>
                <Table
                    key={item._id}
                    index={item._id}
                    email={item.data.email}
                    age={item.data.age}
                    update={this.updateText}
                    deleteBlock={this.deleteBlock} />
            </div>
        );
    };
    
    render() {
        if (!this.state.users.length) return null
        return (
            <div className="container">
               <div >{this.state.users.map(this.eachTask)}</div>
            <Button className="button-o"
              color="primary"
              onClick={this.add.bind(null, 'Email', 'Age')} >Add</Button>
            </div>
        );
    }
}

export default App;
